Thread.abort_on_exception = true;

require 'json';
require 'socket';

require 'hobbes/communication';

require 'server/context';

class Server
  attr_reader :thread;

  def process(object)
    @proc.(object);
  end

  def listen
    @server = TCPServer.new(@host, @port);

    log "Server thread listening on #{@host}:#{@port}";
    @thread = Thread.new{
      loop{
        context = Context.new(@server.accept);
        Thread.new{
          begin
            context.write(process(context));
            context.close;
          rescue Exception => e
            puts "Exception: #{e}";
            begin
              context.send(**Hobbes::Communication.exception(e));
              context.close;
            rescue Exception => e
              puts "Could not close context: #{e}";
            end
          end
        }
      }
    }
  end

  def initialize(host, port)
    @host = host;
    @port = port;
    @proc = proc;
  end
end
