class Server
  class Server::Context
    attr_reader :headers, :request, :ip, :path, :body, :command;

    def send(body: "", status: 200)
      send_header(status);
      send_body(body);
    end

    def close
      send if !@header_sent;
      @socket.close;
    end

    def write(data)
      send if !@header_sent;
      @socket.write(data);
    end

    private

    def send_header(status="200 OK")
      @header_sent = true;
      @socket.print("HTTP/1.1 #{status}\r\n\r\n");
    end

    def send_body(body="")
      @socket.print("#{body}");
    end

    def read_body(socket)
      recv_bytes = @headers['content-length'];
      return if recv_bytes.nil?;
      recv_bytes = recv_bytes.to_i;
      raise "Message size too large (#{recv_bytes})." if recv_bytes > 1e6; # Max 1MB message size.
      @body = socket.read(recv_bytes);
    end

    def read_headers(socket)
      headers = {};

      while true;
        header = socket.gets;
        break if header.nil? or header.strip.empty?;
        key = header[/^[^:]*/].downcase;
        value = header.sub(/^[^:]*:/, "").strip;
        headers[key] = value;
      end

      return headers;
    end

    def initialize(socket)
      @socket = socket;
      @request = socket.gets;
      return if @request.nil?;
      @request = @request.strip;
      @headers = read_headers(socket);
      @body = read_body(socket);

      @header_sent = false;

      @path = @request.split(' ')[1];

      @command = @path[1..-1].split('_');

      # Determine IP behind proxy or not.
      # This is a serious security vulnerability in case you are not behind a proxy.
      @ip = headers['x-real-ip'] || socket.peeraddr.last;
    end
  end
end
