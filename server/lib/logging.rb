#!/usr/bin/env ruby

def log(*args)
  puts *args;
end

def log_error(*args)
  print "[31m";
  log_event(*args);
  print "[m";
end

$log_events = [];

def log_event(*args)
  $log_events << args;
  print "[1m";
  puts *args;
  print "[m";
  return args.join("\n");
end
