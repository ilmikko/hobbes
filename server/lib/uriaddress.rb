#!/usr/bin/env ruby

class URIAddress < URI::HTTP
  include URI

  attr_accessor :scheme, :host, :port, :path;

  def self.local?(ip)
    return true if ip == "localhost" or ip == "::1" or ip == "[::1]" or ip == "127.0.0.1";
    return false;
  end

  def self.parse_2(string)
    if string[/^\w*:\/\//].nil?;
      string = "x://#{string}";
      uri = URI.parse(string);
      uri.scheme = nil;
      return URIAddress.new(uri);
    end

    return URIAddress.new(URI.parse(string));
  end

  def self.rebuild(uri)
    return URIAddress.new(URI.parse("#{uri.scheme}://#{uri.host}:#{uri.port}#{uri.path}"));
  end

  def self.parse(string)
    return nil if string.nil?;

    return rebuild(string) if string.is_a? URI;

    string = 'localhost' if string == '::1';

    uri = parse_2(string);

    if self.local? uri.host
      uri.host = 'localhost';
      uri.scheme = 'http';
    else
      uri.scheme = 'https';
    end

    return URIAddress.new(uri);
  end

  def to_s
    "#{scheme}://#{host}:#{port}#{path}";
  end

  def initialize(uri)
    @host = uri.host;
    @port = uri.port;
    @scheme = uri.scheme;
    @path = uri.path;
  end
end
