$HOBBES_SOCK = "/tmp/hobbes.port";

require 'logging';
require 'server';
require 'uriaddress';

require 'hobbes/address';
require 'hobbes/communication';
require 'hobbes/context';
require 'hobbes/events';
require 'hobbes/hosts';
require 'hobbes/services';

class Hobbes
  include Hobbes::Events

  attr_reader :name, :address, :hosts, :services, :server;

  def decide(context)
    if context.data.empty?
      return Hobbes::Communication.id;
    end

    if not @hosts.known? context.from_address
      raise log_event("Unknown peer: #{context.from_address}");
    end

    puts "[34;1mEVENT <#{context.type}>[m";

    case context.type
    when "echo"
      return echo context;
    when "hello"
      return hello context;
    when "hop"
      return hop context;
    when "run"
      return run context;
    when "sync"
      return sync context;
    else
      raise "Unknown type: #{context.type}";
    end
  end

  def initialize(hosts = [], host: nil, port: nil, address: nil, name: nil)
    @name = name;
    @name = "HOBBES_#{port}" if name.nil?;

    @commands = {
      auth: -> (host) {
        next @hosts.authorize URIAddress.parse(host);
      },
      broadcast: -> (name, path='', context: nil) {
        next @services.broadcast(name, context.body, path: path);
      },
      hop: -> (limit = nil, batch = 1) {
        next @comms.hop({ type: "echo", message: "Good evening" }, limit: limit, batch: batch);
      },
      knows: -> (*peers) {
        r = [];
        peers.each{ |peer|
          r << @hosts.known?(URIAddress.parse(peer));
        };
        next r.join("\n");
      },
      link: -> (host) {
        next @comms.link URIAddress.parse(host);
      },
      list: -> {
        next @hosts.reliability.map{ |k, v| "#{k} #{v}" }.sort.join("\n");
      },
      log: -> {
        next $log_events.join("\n");
      },
      name: -> {
        next @name;
      },
      names: -> {
        next @hosts.names.map{ |address, name| "#{name}: #{address}" }.sort.join("\n");
      },
      providers: -> {
        next @services.providers.map{ |k, v| "#{k}: #{v.keys.map{ |s| URIAddress.parse(s).to_s }.sort.join(" ")}" }.sort.join("\n");
      },
      register: -> (name, host) {
        next @services.register(name, URIAddress.parse(host));
      },
      run: -> (name, *path, context: nil) {
        next @services.run(name, context.body, path: "/#{path.join('_')}");
      },
      services: -> {
        next @services.services.join("\n");
      },
      trust: -> (context: nil) {
        peer = URIAddress.parse(context.ip);
        next "#{@hosts.known?peer} #{peer}";
      },
      unregister: -> (host) {
        next @services.unregister(URIAddress.parse(host));
      },
      version: -> {
        next $version;
      },
    }

    log "Initializing #{@name}...";

    @server = Server.new(host, port){ |context|
      if URIAddress.local? context.ip or $DEBUG
        if not context.command.empty?
          first = context.command.shift.to_sym;

          raise "Unknown command." if @commands[first].nil?;

          action = @commands[first];
          if action.parameters.include?([:key, :context]);
            next action.(*context.command, context: context);
          else
            next action.(*context.command);
          end
        end
      end

      next decide(Context.from_server_context(context));
    };

    @address = Address.new(address);

    @address.host = host if @address.host.nil?;
    @address.port = port if @address.port.nil?;

    @hosts = Hosts.new(URIAddress.parse(@address.to_s), @name);
    @comms = Communication.new(self);

    @services = Services.new(@comms);

    @server.listen;

    File.write($HOBBES_SOCK, port);

    hosts.each{ |peer| @hosts.link URIAddress.parse(peer); };

    @server.thread.join;
  end
end
