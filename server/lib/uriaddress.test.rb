def assert_equals(a, b, message: "Expected ")
  if a != b
    print "A: ";
    p a;
    print "B: ";
    p b;
    raise "#{message} '#{a}' == '#{b}'";
  end
end

def assert_keys(a, b)
  b.each{ |k, v|
    assert_equals(a.send(k), v, message: "Expected key '#{k}' to be the same");
  };
end

load 'lib/uriaddress.rb';

assert_keys(URIAddress.parse('https://1.2.3.4:80/'), { host: '1.2.3.4', port: 80, scheme: 'https', path: '/' });
assert_keys(URIAddress.parse('https://1.2.3.4:80'), { host: '1.2.3.4', port: 80, scheme: 'https', path: '' });
assert_keys(URIAddress.parse('https://1.2.3.4'), { host: '1.2.3.4', scheme: 'https', path: '' });

# External IPs should always be implicitly https.
assert_keys(URIAddress.parse('1.2.3.4:80'), { host: '1.2.3.4', port: 80, scheme: 'https' });
assert_keys(URIAddress.parse('1.2.3.4'), { host: '1.2.3.4', path: '', scheme: 'https' });
assert_keys(URIAddress.parse(':80'), { host: nil, port: 80, path: '' });

# Local IPs should always be implicitly http.
assert_keys(URIAddress.parse('::1'), { host: 'localhost', path: '', scheme: 'http' });
assert_keys(URIAddress.parse('[::1]:443'), { host: 'localhost', port: 443, path: '', scheme: 'http' });
assert_keys(URIAddress.parse('localhost'), { host: 'localhost', path: '', scheme: 'http' });
assert_keys(URIAddress.parse('127.0.0.1/path'), { host: 'localhost', path: '/path', scheme: 'http' });

assert_keys(URIAddress.parse(URI.parse('http://localhost:80')), { host: 'localhost', port: 80, scheme: 'http' });

assert_equals(URIAddress.parse(URI.parse('https://localhost:443')).to_s, "https://localhost:443");
