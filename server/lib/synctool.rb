class Synctool
  def self.sync(to, from)
    self._sync(to, from);
    self.clean(to);
  end

  def self._sync(to, from)
    if to.is_a? Hash and from.is_a? Hash
      from.each{ |k, v|
        to[k] = from.class.new if to[k].nil?;
        to[k] = self._sync(to[k], from[k]);
      };
    else
      return from;
    end
    return to;
  end

  def self.clean(to)
    if to.is_a? Hash
      to.each{ |k, v|
        to.delete(k) if self.clean(v).nil?;
      };
      return nil if to.empty?;
    end
    return nil if to.nil?;
    return true;
  end
end
