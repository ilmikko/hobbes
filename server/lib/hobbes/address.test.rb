def assert_equals(a, b, message: "Expected ")
  if a != b
    print "A: ";
    p a;
    print "B: ";
    p b;
    raise "#{message} '#{a}' == '#{b}'";
  end
end

def assert_keys(a, b)
  b.each{ |k, v|
    assert_equals(a.send(k), v, message: "Expected key '#{k}' to be the same");
  };
end

load 'lib/uriaddress.rb';
load 'lib/hobbes/address.rb';

assert_equals(Hobbes::Address.complete(URIAddress.parse("1.2.3.4"), URIAddress.parse("localhost:1234")).to_s, "https://1.2.3.4:1234");
assert_equals(Hobbes::Address.complete(URIAddress.parse("1.2.3.4"), URIAddress.parse("localhost:33/path")).to_s, "https://1.2.3.4:33/path");
