require 'json';

require 'uriaddress';

class Hobbes
  class Hobbes::Context
    attr_reader :data, :from_address, :return_address, :type, :error;

    def self.parse_json(string)
      return {} if string.nil? or string.empty?;
      begin
        string = "{ \"type\": \"text\", \"text\": #{string.to_json} }" if string[/^{/].nil?
        object = JSON.parse(string);
        return object if object.is_a? Hash;
        if object == true;
          return { "type" => "OK" };
        elsif object == false;
          return { "type" => "NO" };
        elsif object.is_a? Array
          return { "type" => "array", "list" => object };
        else
          raise "Unknown JSON object: #{object.class}";
        end
      rescue JSON::ParserError
        p "Failed to parse json '#{string}'";
        return {};
      end
    end

    def self.from_post(post, address: nil)
      data = parse_json(post.body);

      return_address = URIAddress.parse(data['return']);
      from_address = Address.complete(URIAddress.parse(address), URIAddress.parse(data['from']));

      new(
        from_address: from_address,
        return_address: return_address,
        data: data,
      );
    end

    def self.from_server_context(server_context)
      data = parse_json(server_context.body);

      return_address = URIAddress.parse(data['return']);
      from_address = Address.complete(URIAddress.parse(server_context.ip), URIAddress.parse(data['from']));

      new(
        from_address: from_address,
        return_address: return_address,
        data: data,
      );
    end

    def to_s
      return @data['text'] if @data['text'];
      return @data.to_json;
    end

    def [](key)
      return @data[key];
    end

    def clone(data)
      return Hobbes::Context.new(from_address: @from_address, return_address: @return_address, error: @error, data: data);
    end

    def initialize(from_address: nil, return_address: nil, error: nil, data: {})
      @from_address = from_address if not from_address.nil?;
      @return_address = return_address if not return_address.nil?;

      @data = data;

      @type = data['type'];
      @error = "[#{data['error']}]: #{data['info']}" if @type == 'error';

      @type = type if not type.nil?;
      @error = error if not error.nil?;
    end
  end
end
