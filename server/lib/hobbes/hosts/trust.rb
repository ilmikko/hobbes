require 'resolv';

class Hobbes
  class Hosts
    class Trust
      def parse_host(address)
        return "#{Resolv.getaddress(address.host)}:#{address.port}"
      end

      def host?(address)
        host = parse_host(address);
        return false if @trust[host].nil?;
        return @trust[host] > 0;
      end

      def add(address)
        # All requests from a single host are trusted if an address is trusted.
        # We will resolve the trust to IP addresses from hostnames.
        host = parse_host(address);
        log_event("Trusting host #{host}");
        @trust[host] = 0 if @trust[host].nil?;
        @trust[host] += 1;
      end

      def remove(address)
        host = parse_host(address);
        @trust[host] -= 1 if not @trust[host].nil?;
      end

      def initialize
        @trust = {};
      end
    end
  end
end
