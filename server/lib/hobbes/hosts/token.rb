# Ensures peers are trusted.
require 'resolv';

class Hobbes
  class Hobbes::Hosts
    class Hobbes::Hosts::Token
      def parse_host(address)
        return "#{Resolv.getaddress(address.host)}:#{address.port}"
      end

      def new(address)
        host = parse_host(address);
        @tokens[host] = Time.now.to_i;
        return true;
      end

      def valid?(address)
        host = parse_host(address);
        return false if @tokens[host].nil?;
        return false if Time.now.to_i - @tokens[host] > @token_valid_time;
        return true;
      end

      def initialize(seconds_valid: 60)
        @token_valid_time = seconds_valid;
        @tokens = {};
      end
    end
  end
end

