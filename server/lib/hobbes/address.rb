#!/usr/bin/env ruby

require 'ipaddr';

class Hobbes
  class Address
    def self.complete(real_address, fake_address)
      # This method exists to attach extra information to the real address.
      # In no case should we rewrite real_address, as this is coming from a trusted source.
      #
      # However, we can read extra information from fake_address (provided by the client),
      # mainly the alleged port and path the other service is running on.
      # Yes, this information can be spoofed, however doing so does not change our trust towards the client, as trust is based on the real_address alone (mainly, IP address).
      #
      # Here fake_address is used to provide extra information about which Hobbes instance (in case there are multiple running on the same server) we are allowing.

      return real_address if fake_address.nil?;

      real_address.port = fake_address.port if real_address.port.nil?;
      real_address.path = fake_address.path if real_address.path.nil? or real_address.path.empty?;

      return real_address;
    end

    # Our world address.
    @host = nil;
    @port = nil;
    @path = nil;

    attr_accessor :host, :port, :path;

    def to_s
      "#{@host}:#{@port}#{@path}";
    end

    def initialize(self_address)
      if not self_address.nil?
        addr = URIAddress.parse(self_address);

        @host = addr.host;
        @port = addr.port;
        @path = addr.path;
      end
    end
  end
end
