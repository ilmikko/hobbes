require 'hobbes/communication/core';
require 'hobbes/communication/types';

class Hobbes
  class Hobbes::Communication
    attr_reader :hobbes;

    include Hobbes::Communication::Core;

    def link(address)
      begin
        @hobbes.hosts.add_peer(address);
        send({
          type: 'hello',
          name: @hobbes.name,
        }, address);
        sync();
        return true;
      rescue Exception => e
        log_error("Linking failed with #{address}: #{e}");
        @hobbes.hosts.remove_peer(address);
        # raise e
        return false;
      end
    end

    def sync(only: [:peers, :providers])
      object = {
        'type' => 'sync',
      };

      object['peers'] = @hobbes.hosts.names if only.include? :peers;
      object['providers'] = @hobbes.services.providers if only.include? :providers;

      broadcast(object);
    end

    def run(name, path, data, peers)
      hop({
        'type' => 'run',
        'name' => name,
        'path' => path,
        'data' => data,
      }, limit: 1, peers: peers);
    end

    def initialize(hobbes)
      @hobbes = hobbes;
    end
  end
end
