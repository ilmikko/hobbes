require 'json';

require 'synctool';

class Hobbes
  class Hobbes::Services
    def broadcast(name, data, path: '')
      raise "Tried to run unknown service: #{name}" if @providers[name].nil?;

      data = "" if data.nil?;
      data = data.to_json if not data.is_a? String;

      # Broadcast to global instances.
      @comms.broadcast({
        'name' => name,
        'path' => path,
        'data' => data,
      }, peers: @providers[name].keys);

      # Broadcast to local instances.
      if not @services[name].nil?
        @services[name].keys.each{ |peer|
          address = URIAddress.parse(peer);
          address.path = path if !path.empty?;
          begin
            @comms.post(address, data);
          rescue Exception => e
            next
          end
        };
      end
    end

    def register(name, *addresses)
      # TODO: Check for inconsistent states here
      addresses.each{ |address|
        if not @comms.check(address);
          log_error("Failed to register #{name} service on #{address}!");
          return false;
        end

        add_service(name, address);
        add(name, @comms.hobbes.address.to_s);
      };
      @comms.sync(only: [:providers]);
      return true;
    end

    def run(name, data, path: '')
      raise "Tried to run unknown service: #{name}" if @providers[name].nil?;

      data = "" if data.nil?;
      data = data.to_json if not data.is_a? String;

      if @services[name].nil?
        # We need to send a request to one of the providers.
        return @comms.run(name, path, data, @providers[name].keys);
      end

      # We have a local instance.
      peer = @services[name].map{ |k, _| k }.sample;
      address = URIAddress.parse(peer);
      address.path = path if !path.empty?;
      return @comms.post(address, data);
    end

    def providers
      @providers
    end

    def services
      @providers.keys;
    end

    def sync(providers)
      Synctool.sync(@providers, providers);
    end

    def unregister(*addresses)
      addresses.each{ |address|
        remove_service(address);
      };
      @comms.sync(only: [:providers]);
      sync(@providers);
      return true;
    end

    private

    def add(name, *providers)
      @providers[name] = {} if @providers[name].nil?;
      providers.each{ |provider|
        @providers[name][provider] = 1;
      };
    end

    def add_service(name, *addresses)
      @services[name] = {} if @services[name].nil?;
      addresses.each{ |address|
        @services[name][address] = 1;
        @names[address] = name;
      };
    end

    def remove_service(address)
      raise "Tried to remove unknown service address: #{address}" if @names[address].nil?;

      name = @names[address];
      @names.delete(address);

      # Remove ourselves from the providers.
      @providers[name][@comms.hobbes.address.to_s] = nil;
      # Remove the service from our services.
      @services[name][address] = nil;
    end

    def initialize(comms)
      @comms = comms;

      # Contains all local services.
      @services = {};

      # Contains mappings to find all services in the Hobbes network.
      @providers = {};
      # Reverse map of address to service names
      @names = {};
    end
  end
end
