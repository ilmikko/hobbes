class Hobbes
  module Events
    def echo(context)
      puts "E: #{context['message']} from #{context.from_address}";
      return true;
    end

    def hello(context)
      from_address = context.from_address;
      return_address = context.return_address;

      p "Hello from #{return_address} (addr #{from_address})"
      @hosts.add_peer(return_address);
      @hosts.name(return_address, context['name']);

      # Broadcast the new peer information to all my peers.
      return @comms.sync();
    end

    def hop(context = nil)
      puts "Received hop from #{context.from_address}";

      batch = context['batch'];
      hops = context['hops'];
      peers = context['peers'];
      limit = context['limit'];
      limit -= 1 if not limit.nil?;

      r = decide(context.clone(context['message']));
      h = @comms.hop(context['message'], hops: hops, batch: batch, limit: limit, peers: peers);

      if h.nil?
        return r
      else
        return h
      end
    end

    def run(context)
      name = context['name'];
      path = context['path'];
      data = context['data'];

      @services.run(name, data, path: path);
    end

    def sync(context)
      from_address = context.from_address;

      log_event("Received sync event from #{from_address}");

      if not context['peers'].nil?
        log_event("Received new peers from #{from_address}: #{context['peers']}");
        context['peers'].each{ |address, name|
          address = URIAddress.parse(address);
          @hosts.add_peer(address);
          @hosts.name(address, name) if name;
        };
      end

      if not context['providers'].nil?
        log_event("Received providers from #{from_address}: #{context['providers']}");
        # Convert addresses to the right format.
        @services.sync(context['providers']);
      end
    end
  end
end
