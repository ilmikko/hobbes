class Hobbes
  class Hobbes::Communication
    def self.id; "Hobbes"; end

    def self.exception(e)
      error = { type: 'error', error: "Bad Request" };
      error['info'] = e if $DEBUG == true;

      return { body: error.to_json, status: 400 };
    end
  end
end
