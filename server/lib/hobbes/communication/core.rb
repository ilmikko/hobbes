require 'net/http';

class Hobbes
  class Hobbes::Communication
    module Hobbes::Communication::Core
      def broadcast(object, peers: @hobbes.hosts.peers)
        puts "Broadcasting #{object}";
        puts "Peer size: #{@hobbes.hosts.peers.size}";
        # TODO: Currently broadcast is limited to 1 to avoid concurrency.
        # The same with hop with batch size of 1 to avoid the same problem.
        # We will need to solve concurrency at some point but it becomes
        # a bit of a painful graph problem, and will probably require a heavy
        # modification to the hopping algorithm.
        hop(object, batch: @hobbes.hosts.peers.size, limit: 1, peers: peers);
      end

      def hop(object = {}, batch: 1, limit: nil, hops: [], peers: @hobbes.hosts.peers)
        return nil if !limit.nil? and limit <= 0;

        own_address = URIAddress.parse(@hobbes.address.to_s).to_s;
        hops << own_address if !hops.include? own_address;

        hop = {
          type: 'hop',
          hops: hops,
          peers: peers,
          limit: limit,
          batch: batch,
          message: object,
        };

        hop[:batch] = 1 if hop[:batch].nil?;

        next_peer_pool = peers - hops;

        r = [];
        batch = hop[:batch];
        while batch > 0
          puts "BATCH: #{batch}";
          if next_peer_pool.empty?
            # This is fine if limit was not defined.
            # However, if limit was defined, we have ran out of hosts to hop through.
            raise "Peer pool is not large enough for #{batch} hops!" if not limit.nil?;
            # Debug print yay!
            puts "Hops: #{hops}";
            return hops.map{ |peer| "#{peer} (#{@hobbes.hosts.name(URIAddress.parse(peer))})" }.join(" -> ");
          end

          peer = next_peer_pool.sample;
          next_peer_pool -= [ peer ];

          address = URIAddress.parse(peer);
          begin
            puts "Sending hop to address (#{batch}): #{address}";
            r << send(hop, address);
            @hobbes.hosts.peer_works(address);
            batch -= 1;
          rescue Exception => e
            log_error "Could not send to #{address}: #{e}";
            @hobbes.hosts.peer_failed(address);
          end
        end
        return r.join("\n");
      end

      def reraise(context)
        # Reraise errors.
        raise context.error if context.type == 'error';
        return context;
      end

      def send(object, address)
        object['return'] = @hobbes.address if object['return'].nil?;
        object['from'] = @hobbes.address if object['from'].nil?;

        puts "SEND #{object.to_json} TO #{address}";

        return reraise(
          Hobbes::Context.from_post(
            Net::HTTP.post(URIAddress.parse(address), object.to_json),
            address: address
          )
        );
      end

      def post(address, data)
        return Net::HTTP.post(URIAddress.parse(address), data).body
      end

      def check(address)
        begin
          Net::HTTP.post(URIAddress.parse(address), "");
          return true;
        rescue Exception => e
          log_error("Peer check failure for address #{address}: #{e}");
          return false;
        end
      end
    end
  end
end
