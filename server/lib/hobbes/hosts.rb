require 'hobbes/hosts/token';
require 'hobbes/hosts/trust';

class Hobbes
  class Hobbes::Hosts
    @@max_reliability = 5

    attr_reader :names, :reliability;

    def add_peer(*peers)
      peers.each{ |address|
        key = address.to_s;
        if not @names.key? key;
          log_event("Added peer #{key}");
          @names[key] = nil;
          @reliability[key] = @@max_reliability;
          @trust.add(address);
        end
      };
    end

    def all
      @names.keys;
    end

    def authorize(address)
      log_event("Authorizing peer #{address}");
      return @token.new address;
    end

    def known? address;
      @trust.host? address or @token.valid? address;
    end

    def name(address, name = nil)
      raise "Unknown address (name): #{address}" if not known? address;

      key = address.to_s;
      return @names[key] if name.nil?;
      puts "SET #{key} AS #{name}";
      @names[key] = name;
    end

    def peers
      all - [ @self ];
    end

    def peer_failed(address)
      key = address.to_s;
      return if @reliability[key].nil?;

      @reliability[key] -= 1;
      if @reliability[key] <= 0
        remove_peer(address);
      end
    end

    def peer_works(address)
      key = address.to_s;
      @reliability[key] = @@max_reliability;
    end

    def remove_peer(*peers)
      peers.each{ |address|
        key = address.to_s;

        if @names.key? key;
          log_event("Removed peer #{key}");
          @names.delete(key);
          @reliability.delete(key);
          @trust.remove(address);
        end
      };
    end

    def initialize(self_address, self_name)
      @names = {};
      @reliability = {};
      @self = self_address.to_s;

      @token = Token.new;
      @trust = Trust.new;

      add_peer(self_address);
      name(self_address, self_name) if not self_name.nil?;
    end
  end
end
