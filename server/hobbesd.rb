#!/usr/bin/env ruby

$DIR = File.dirname(__FILE__);
$LOAD_PATH << "#{$DIR}/lib";

require 'hobbes';

def fail(*args)
  log(*args);
  exit 1;
end

hosts = [];
host = 'localhost';
port = 10000 + rand(10000);

$version = "#{`git tag 2>/dev/null`.strip}-#{`git rev-parse HEAD 2>/dev/null`.strip[-6..-1]}";
puts "Hobbes version: #{$version}";

while !ARGV.empty?
  arg = ARGV.shift;
  case arg
  when "--address"
    address = ARGV.shift;
  when "--host"
    host = ARGV.shift;
  when "--port"
    port = ARGV.shift;
  when "--name"
    name = ARGV.shift;
  when "--debug"
    $DEBUG=true;
    puts "Hobbes was launched in debug mode.";
    puts "Please do not use this in production!";
    debug_files = [];
    while !ARGV.first.nil? and ARGV.first[/^--/].nil?
      debug_files << ARGV.shift;
    end

    Thread.new{
      debug_files.each{ |file|
        puts "Debug: Loading #{file}";
        load "#{file}";
      };
    }
  when /^--/
    fail "Unknown command: #{arg}";
  else
    hosts << arg;
  end
end

Hobbes.new(hosts, host: host, port: port, name: name, address: address);
