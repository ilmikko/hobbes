PORT=25555;
HOST=localhost;

kill_all() {
	kill $(jobs -p);
}

exit() {
	kill_all;
	________; # what?
	command exit "$@";
}

get() {
	curl --silent "$@";
}

./hobbesd.rb --port "$((PORT + 1))" --name Munich &
./hobbesd.rb --port "$((PORT + 2))" --name Mombasa &
./hobbesd.rb --port "$((PORT + 3))" --name Reykjavik &

MUNICH="$HOST:$((PORT + 1))";
MOMBASA="$HOST:$((PORT + 2))";
REYKJAVIK="$HOST:$((PORT + 3))";

wait_for_server() {
	tries=20;
	while ! get "$1" >/dev/null; do
		tries=$((tries - 1));
		if ! [ "$tries" -gt 0 ]; then
			kill_all;
			exit 1;
		fi
		sleep 0.1;
	done
}

# Wait for all servers to come online.
wait_for_server "$MUNICH";
wait_for_server "$MOMBASA";
wait_for_server "$REYKJAVIK";

# Check that all servers are sane.
assert_equals "$(get "$MUNICH")" "Hobbes";
assert_equals "$(get "$MOMBASA")" "Hobbes";
assert_equals "$(get "$REYKJAVIK")" "Hobbes";
assert_equals "$(get "$MUNICH/name")" "Munich";
assert_equals "$(get "$MOMBASA/name")" "Mombasa";
assert_equals "$(get "$REYKJAVIK/name")" "Reykjavik";

# Check that these start unlinked.
assert_equals "$(get "$MUNICH/list")" "http://$MUNICH 5";
assert_equals "$(get "$MOMBASA/list")"   "http://$MOMBASA 5";
assert_equals "$(get "$REYKJAVIK/list")" "http://$REYKJAVIK 5";

# Link Mombasa to Munich
assert_equals "$(get "$MUNICH/auth_$MOMBASA")" "true";
assert_equals "$(get "$MOMBASA/link_$MUNICH")" "true";

# Link Reykjavik to Mombasa
assert_equals "$(get "$MOMBASA/auth_$REYKJAVIK")" "true";
assert_equals "$(get "$REYKJAVIK/link_$MOMBASA")" "true";

# Test that the link works.
! ! assert_equals "$(get "$MUNICH/list")" "$(cat <<HERE
http://$MUNICH 5
http://$MOMBASA 5
http://$REYKJAVIK 5
HERE
)" || exit 1;
! ! assert_equals "$(get "$MOMBASA/list")" "$(cat <<HERE
http://$MUNICH 5
http://$MOMBASA 5
http://$REYKJAVIK 5
HERE
)" || exit 1;
! ! assert_equals "$(get "$REYKJAVIK/list")" "$(cat <<HERE
http://$MUNICH 5
http://$MOMBASA 5
http://$REYKJAVIK 5
HERE
)" || exit 1;

# Test that names have been propagated correctly.
! ! assert_equals "$(get "$MUNICH/names")" "$(cat <<HERE
Mombasa: http://$MOMBASA
Munich: http://$MUNICH
Reykjavik: http://$REYKJAVIK
HERE
)" || exit 1;
! ! assert_equals "$(get "$MOMBASA/names")" "$(cat <<HERE
Mombasa: http://$MOMBASA
Munich: http://$MUNICH
Reykjavik: http://$REYKJAVIK
HERE
)" || exit 1;
! ! assert_equals "$(get "$REYKJAVIK/names")" "$(cat <<HERE
Mombasa: http://$MOMBASA
Munich: http://$MUNICH
Reykjavik: http://$REYKJAVIK
HERE
)" || exit 1;

# There are two ways to hop through the peers.
# MUNICH -> MOMBASA -> REYKJAVIK
# or
# MUNICH -> REYKJAVIK -> MOMBASA
# Test that it is one of them, and the names are correct on the hops.
hop="$(get "$MUNICH/hop")"
! ! assert_equals "$hop" "http://$MUNICH (Munich) -> http://$MOMBASA (Mombasa) -> http://$REYKJAVIK (Reykjavik)" || ! ! assert_equals "$hop" "http://$MUNICH (Munich) -> http://$REYKJAVIK (Reykjavik) -> http://$MOMBASA (Mombasa)" || exit 1;

# And everyone lived happily ever after.
kill_all;
