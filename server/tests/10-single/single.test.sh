PORT=25555;
HOST=localhost;
ADDR="$HOST:$PORT";

exit() {
	kill $(jobs -p);
	command exit $@;
}

server_init() {
	./hobbesd.rb --port "$PORT" "$@" &
	PID=$!;
	# Wait for the server to go online.
	# Wait for both to come online.
	tries=20;
	while ! curl --silent "$ADDR" >/dev/null; do
		tries=$((tries - 1));
		if ! [ "$tries" -gt 0 ]; then
			kill_all;
			exit 1;
		fi
		sleep 0.1;
	done
}

server_kill() {
	kill $PID;
}

test_hobbes_port() {
	assert_equals "$(curl --silent "$ADDR")" "";
	server_init;
	assert_equals "$(curl --silent "$ADDR")" "Hobbes";
	server_kill;
}

test_hobbes_local_name() {
	assert_equals "$(curl --silent "$ADDR")" "";

	# Default name is HOBBES_<port>.
	server_init;
	assert_equals "$(curl --silent "$ADDR/name")" "HOBBES_$PORT";
	server_kill;

	# Test that passed name gets used.
	server_init --name "Zurich";
	assert_equals "$(curl --silent "$ADDR/name")" "Zurich";
	server_kill;
}

test_hobbes_local_hosts() {
	assert_equals "$(curl --silent "$ADDR")" "";

	server_init --name "Zurich";
	assert_equals "$(curl --silent "$ADDR/list")" "http://$HOST:$PORT 5";
	server_kill;
}

test_hobbes_single_hop() {
	assert_equals "$(curl --silent "$ADDR")" "";

	# HTTP is used for local hosts.
	server_init --name "Zurich";
	assert_equals "$(curl --silent "$ADDR/hop")" "http://$HOST:$PORT (Zurich)";
	server_kill;
}
