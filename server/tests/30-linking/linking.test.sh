PORT=25555;
HOST=localhost;

kill_all() {
	kill $(jobs -p);
}

exit() {
	kill_all;
	________; # what?
	command exit "$@";
}

get() {
	curl --silent "$@";
}

./hobbesd.rb --debug --port "$((PORT + 1))" --name Zurich &
./hobbesd.rb --debug --port "$((PORT + 2))" --name Oslo &
./hobbesd.rb --debug --port "$((PORT + 3))" --name Hawaii &

ZURICH="$HOST:$((PORT + 1))";
OSLO="$HOST:$((PORT + 2))";
HAWAII="$HOST:$((PORT + 3))";

wait_for_server() {
	tries=20;
	while ! get "$1" >/dev/null; do
		tries=$((tries - 1));
		if ! [ "$tries" -gt 0 ]; then
			kill_all;
			exit 1;
		fi
		sleep 0.1;
	done
}

# Wait for all servers to come online.
wait_for_server "$ZURICH";
wait_for_server "$OSLO";
wait_for_server "$HAWAII";

# Check that all servers are sane.
assert_equals "$(get "$ZURICH")" "Hobbes";
assert_equals "$(get "$OSLO")" "Hobbes";
assert_equals "$(get "$HAWAII")" "Hobbes";
assert_equals "$(get "$ZURICH/name")" "Zurich";
assert_equals "$(get "$OSLO/name")" "Oslo";
assert_equals "$(get "$HAWAII/name")" "Hawaii";

# Check that these start unlinked.
assert_equals "$(get "$ZURICH/list")" "http://$ZURICH 5";
assert_equals "$(get "$OSLO/list")"   "http://$OSLO 5";
assert_equals "$(get "$HAWAII/list")" "http://$HAWAII 5";

# Try linking Oslo to Zurich.
# This should fail, as Zurich does not know who Oslo is.
assert_equals "$(get "$OSLO/link_$ZURICH")" "false";
# Same for the other way around.
assert_equals "$(get "$ZURICH/link_$OSLO")" "false";

# What we need to do is authorize Zurich to accept Oslo.
# The authorization is only done from a localhost command.
# This implies external authentication as we are in the local network.
# We do this now.
assert_equals "$(get "$ZURICH/auth_$OSLO")" "true";

# Test that, even though Zurich just authorized Oslo, Hawaii cannot link.
assert_equals "$(get "$HAWAII/link_$ZURICH")" "false";
assert_equals "$(get "$HAWAII/link_$OSLO")" "false";

# Test that, even though Zurich just authorized Oslo, Zurich cannot link to Oslo
# (as Oslo didn't authorize Zurich).
assert_equals "$(get "$ZURICH/link_$OSLO")" "false";

# But now when we link Oslo to Zurich, Zurich accepts.
assert_equals "$(get "$OSLO/link_$ZURICH")" "true";

# Check that Zurich and Oslo are now linked.
! ! assert_equals "$(get "$ZURICH/list")" "$(cat <<HERE
http://$ZURICH 5
http://$OSLO 5
HERE
)" || exit 1;
! ! assert_equals "$(get "$OSLO/list")" "$(cat <<HERE
http://$ZURICH 5
http://$OSLO 5
HERE
)" || exit 1;

# As we know from before, Hawaii cannot link to either Oslo or Zurich.
assert_equals "$(get "$HAWAII/link_$ZURICH")" "false";
assert_equals "$(get "$HAWAII/link_$OSLO")" "false";

# Now Oslo authorizes Hawaii.
assert_equals "$(get "$OSLO/auth_$HAWAII")" "true";

# Hawaii tries Zurich - that should fail.
assert_equals "$(get "$HAWAII/link_$ZURICH")" "false";
# But Hawaii linking to Oslo should succeed.
assert_equals "$(get "$HAWAII/link_$OSLO")" "true";

# Because Hawaii linked with Oslo, and Oslo is already linked with Zurich, we should now have a three peer network.
# This means Hawaii can talk to Zurich after linking with Oslo. Everyone is friends!
! ! assert_equals "$(get "$HAWAII/list")" "$(cat <<HERE
http://$ZURICH 5
http://$OSLO 5
http://$HAWAII 5
HERE
)" || exit 1;
! ! assert_equals "$(get "$ZURICH/list")" "$(cat <<HERE
http://$ZURICH 5
http://$OSLO 5
http://$HAWAII 5
HERE
)" || exit 1;
! ! assert_equals "$(get "$OSLO/list")" "$(cat <<HERE
http://$ZURICH 5
http://$OSLO 5
http://$HAWAII 5
HERE
)" || exit 1;

# And everyone lived happily ever after.
kill_all;
