PORT=25555;
HOST=localhost;
ADDR="$HOST:$PORT";

kill_after() {
	sleep $@;
	kill $!;
}

test_debug_message() {
	assert_find "$(./hobbesd.rb --debug --port "$PORT" & kill_after 0.3)" "Hobbes was launched in debug mode." "Please do not use this in production!";
}

test_debug_imports() {
	! ! assert_find "$(./hobbesd.rb --debug "echo.rb" --port "$PORT" & kill_after 0.3)" "$(cat <<-HERE
	Debug: Loading echo.rb
	This line is echoed at the end of the initialization.
	HERE
	)" || exit 1;
}
