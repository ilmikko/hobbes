Contains tests for a hobbes configuration where there are two peers.
Most importantly we test that regular communication works and nodes dying does not crash the system.
