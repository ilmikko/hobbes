PORT=25555;
HOST=localhost;

kill_all() {
	kill $(jobs -p);
}

exit() {
	kill_all;
	sleep 0.1;
	________; # what?
	command exit 1;
}

get() {
	curl --fail --silent "$@";
}

./hobbesd.rb --port "$(( PORT + 1 ))" --name Zurich &
ZURICHPID="$!";
./hobbesd.rb --port "$(( PORT + 2 ))" --name Oslo &
OSLOPID="$!";

ZURICH="$HOST:$(( PORT + 1 ))";
OSLO="$HOST:$(( PORT + 2 ))";

wait_for_server() {
	tries=20;
	while ! get "$1" >/dev/null; do
		tries=$((tries - 1));
		if ! [ "$tries" -gt 0 ]; then
			kill_all;
			exit 1;
		fi
		sleep 0.1;
	done
}

# Wait for both to come online.
wait_for_server "$ZURICH";
wait_for_server "$OSLO";

assert_equals "$(get "$ZURICH")" "Hobbes";
assert_equals "$(get "$OSLO")" "Hobbes";

# Check that these start unlinked.
assert_equals "$(get "$ZURICH/list")" "http://$ZURICH 5";
assert_equals "$(get "$OSLO/list")" "http://$OSLO 5";

# Link Zurich to Oslo.
assert_equals "$(curl "$OSLO/auth_$ZURICH")" "true";
assert_equals "$(curl "$ZURICH/link_$OSLO")" "true";

# Hop once from Oslo to Zurich.
assert_equals "$(curl "$OSLO/hop")" "http://$OSLO (Oslo) -> http://$ZURICH (Zurich)";

# Kill Zurich
kill "$ZURICHPID";

# Hop once from Oslo to Zurich.
! ! assert_equals "$(curl "$OSLO/hop")" "http://$OSLO (Oslo)" || exit 1;

# Check that the server is still active.
assert_equals "$(get "$OSLO")" "Hobbes";

# Check that its list still contains Zurich, but it is more unreliable.
! ! assert_equals "$(get "$OSLO/list")" "$(cat <<HERE
http://$ZURICH 4
http://$OSLO 5
HERE
)" || exit 1;

# Hop three more times, enough to cause four errors and almost drop Zurich from the active hosts.
for t in 1 2 3; do
	! ! assert_equals "$(curl "$OSLO/hop")" "http://$OSLO (Oslo)" || exit 1;
done

# Check that its list still contains Zurich, but it is quite unreliable.
! ! assert_equals "$(get "$OSLO/list")" "$(cat <<HERE
http://$ZURICH 1
http://$OSLO 5
HERE
)" || exit 1;

# One more time.
! ! assert_equals "$(curl "$OSLO/hop")" "http://$OSLO (Oslo)" || exit 1;

# Check that its list does not contain Zurich any longer.
! ! assert_equals "$(get "$OSLO/list")" "$(cat <<HERE
http://$OSLO 5
HERE
)" || exit 1;

# Check that failed connections are logged for debugging purposes.
assert_find "$(get "$OSLO/log")" "Could not send to http://$ZURICH: Failed to open TCP connection to $ZURICH (Connection refused - connect(2) for \"$HOST\" port $(( PORT + 1 )))" "Removed peer http://$ZURICH"

kill_all;
