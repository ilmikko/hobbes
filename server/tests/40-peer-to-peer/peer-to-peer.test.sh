PORT1=25555;
PORT2=25560;
HOST=localhost;

kill_all() {
	kill $(jobs -p);
}

exit() {
	kill_all;
	________; # what?
	command exit "$@";
}

get() {
	curl --silent "$@";
}

./hobbesd.rb --port "$PORT1" --name Zurich &
./hobbesd.rb --port "$PORT2" --name Oslo &

# Wait for both to come online.
tries=20;
while ! get "$HOST:$PORT1" >/dev/null || ! get "$HOST:$PORT2" >/dev/null; do
	tries=$((tries - 1));
	if ! [ "$tries" -gt 0 ]; then
		kill_all;
		exit 1;
	fi
	sleep 0.1;
done

assert_equals "$(get "$HOST:$PORT1")" "Hobbes";
assert_equals "$(get "$HOST:$PORT2")" "Hobbes";

# Check that these start unlinked.
assert_equals "$(get "$HOST:$PORT1/list")" "http://$HOST:$PORT1 5";
assert_equals "$(get "$HOST:$PORT2/list")" "http://$HOST:$PORT2 5";

# Link Zurich to Oslo.
assert_equals "$(curl "$HOST:$PORT2/auth_http://$HOST:$PORT1")" "true";
assert_equals "$(curl "$HOST:$PORT1/link_http://$HOST:$PORT2")" "true";

# Check that the link has been created.
! ! assert_equals "$(get "$HOST:$PORT1/list")" "$(cat <<HERE
http://$HOST:$PORT1 5
http://$HOST:$PORT2 5
HERE
)" || exit 1;
! ! assert_equals "$(get "$HOST:$PORT2/list")" "$(cat <<HERE
http://$HOST:$PORT1 5
http://$HOST:$PORT2 5
HERE
)" || exit 1;

# Check that Zurich hops to Oslo.
assert_equals "$(get "$HOST:$PORT1/hop")" "http://$HOST:$PORT1 (Zurich) -> http://$HOST:$PORT2 (Oslo)";
# And vice versa.
assert_equals "$(get "$HOST:$PORT2/hop")" "http://$HOST:$PORT2 (Oslo) -> http://$HOST:$PORT1 (Zurich)";

kill_all;
