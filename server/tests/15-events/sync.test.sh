PORT=25555;
HOST=localhost;
ADDR="$HOST:$PORT";

exit() {
	kill $(jobs -p);
	command exit $@;
}

get() {
	curl --silent "$@";
}

server_init() {
	./hobbesd.rb --port "$PORT" "$@" &
	PID=$!;
	# Wait for the server to go online.
	# Wait for both to come online.
	tries=20;
	while ! get "$ADDR" >/dev/null; do
		tries=$((tries - 1));
		if ! [ "$tries" -gt 0 ]; then
			kill_all;
			exit 1;
		fi
		sleep 0.1;
	done
}

server_kill() {
	kill $PID;
}

test_hobbes_sync_peers() {
	assert_equals "$(get "$ADDR")" "";

	server_init --name "Zurich";
	assert_equals "$(get --data '{ "from": "'$ADDR'", "type": "sync", "peers": { "https://localhost:1234": "test1", "http://localhost:2345": "test2", "https://127.0.0.1:3456/path": "test3" } }' "$ADDR")" "";
	! ! assert_equals "$(get "$ADDR/list")" "$(cat <<-HERE
	http://localhost:1234 5
	http://localhost:2345 5
	http://localhost:25555 5
	http://localhost:3456/path 5
	HERE
	)" || exit 1;
	server_kill;
}
