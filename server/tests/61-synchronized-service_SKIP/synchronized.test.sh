# This is an important test for the Hobbes distributed system.

# Imagine we have a service that 'simply' increments a counter for users on request.
# That is, a request to Increment/ will return 1. Then 2. Then 3. Then 4...

# It is important that the counter is thus fully synchronized between the server instances.
# This can be done in various ways, but here is one way.

# 1. Request to Increment/
# 2. Increment/ sends a broadcast back through Hobbes to other Increment/ instances, asking if they can increment.
# 3. Other servers return OK
# 4. First server increments. Sends a broadcast back to all other servers to increment.
# 5. First server returns the number.

# -> The above is good, but I think we can do better.
# 1. Request to Increment/
# 2. First server increments
# 3. First server sends a broadcast to other instances to increment
# 4. Returns the number.
# 5. Make sure to sync between the clients, but after returning the result to the client (what happens to clients that are down?)

exit 1;
