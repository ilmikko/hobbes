PORT=25555;
HOST=localhost;

kill_all() {
	kill $(jobs -p);
}

exit() {
	kill_all;
	________; # what?
	command exit "$@";
}

get() {
	curl --silent "$@";
}

wait_for_server() {
	tries=20;
	while ! get "$1" >/dev/null; do
		tries=$((tries - 1));
		if ! [ "$tries" -gt 0 ]; then
			kill_all;
			exit 1;
		fi
		sleep 0.1;
	done
}

./hobbesd.rb --debug --port "$((PORT + 1))" --name Helsinki &

HELSINKI="$HOST:$((PORT + 1))";

# Wait for server
wait_for_server "$HELSINKI";

# Check sanity.
assert_equals "$(get "$HELSINKI")" "Hobbes";
assert_equals "$(get "$HELSINKI/name")" "Helsinki";

# Check that service list is empty by default.
assert_equals "$(get "$HELSINKI/services")" "";

# Try to register a service that is not there.
assert_equals "$(get "$HELSINKI/register_ShoutService_$HOST:$((PORT + 2))")" "false";

# Try to send messages to a service that is not there.
assert_find "$(get --data "shout this string" "$HELSINKI/run_ShoutService")" "error" "Bad Request";

# Start an instance of the shout service.
./shoutservice.rb --port "$((PORT + 2))" &

SHOUT="$HOST:$((PORT + 2))";

wait_for_server "$SHOUT";

# Test that direct communication works.
assert_equals "$(get --data "shout this string" "$SHOUT")" "SHOUT THIS STRING";
assert_equals "$(get --data "WHISPER this string" "$SHOUT/whisper")" "whisper this string";
assert_equals "$(get --data "python is the superior programming language" "$SHOUT/sarcasm")" "pYtHoN Is tHe sUpErIoR PrOgRaMmInG LaNgUaGe";

# Register the shout service
assert_equals "$(get "$HELSINKI/register_ShoutService_$SHOUT")" "true";

# We should now have the service registered.
assert_equals "$(get "$HELSINKI/services")" "ShoutService";

# Send a message to this service
assert_equals "$(get --data "shout this string" "$HELSINKI/run_ShoutService")" "SHOUT THIS STRING";
assert_equals "$(get --data "WHISPER this string" "$HELSINKI/run_ShoutService_whisper")" "whisper this string";
assert_equals "$(get --data "python is the superior programming language" "$HELSINKI/run_ShoutService_sarcasm")" "pYtHoN Is tHe sUpErIoR PrOgRaMmInG LaNgUaGe";

kill_all;
