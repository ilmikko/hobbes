#!/usr/bin/env ruby

require 'socket';

$port = 22222;

while ARGV.size > 0
  case ARGV.shift
  when "--port"
    $port = ARGV.shift;
  else
    raise "Unknown argument: #{$port}";
  end
end

server = TCPServer.new($port);

loop {
  client = server.accept

  request = client.gets;

  # Headers.
  headers = {};
  loop {
    line = client.gets;
    break if line.nil? or line.strip.empty?
    key = line[/^[^:]*/].strip.downcase;
    value = line[/[^:]*$/].strip;
    headers[key] = value;
  }

  size = headers['Content-Length'.downcase];
  data = ""
  if not size.nil?
    size = size.to_i;
    data = client.read(size);
  end

  client.write "HTTP/1.1 200 OK\r\n";
  client.write "\r\n";

  path = request.split(' ')[1]

  case path
  when "/whisper"
    client.write data.downcase;
  when "/sarcasm"
    client.write data.split('').each_with_index.map{ |c, i| i % 2 == 0 ? c.downcase : c.upcase }.join('')
  when /^\/append/
    client.write data + path.split('_')[1..-1].join('_');
  else
    client.write data.upcase;
  end

  client.close;
}
