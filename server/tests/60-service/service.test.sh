PORT=25555;
HOST=localhost;

kill_all() {
	kill $(jobs -p);
}

exit() {
	kill_all;
	________; # what?
	command exit "$@";
}

get() {
	curl --silent "$@";
}

wait_for_server() {
	tries=20;
	while ! get "$1" >/dev/null; do
		tries=$((tries - 1));
		if ! [ "$tries" -gt 0 ]; then
			kill_all;
			exit 1;
		fi
		sleep 0.1;
	done
}

./hobbesd.rb --debug --port "$((PORT + 1))" --name Helsinki &
./hobbesd.rb --debug --port "$((PORT + 2))" --name Stockholm &

HELSINKI="$HOST:$((PORT + 1))";
STOCKHOLM="$HOST:$((PORT + 2))";

# Wait for server
wait_for_server "$HELSINKI";
wait_for_server "$STOCKHOLM";

# Check sanity.
assert_equals "$(get "$HELSINKI")" "Hobbes";
assert_equals "$(get "$STOCKHOLM")" "Hobbes";

assert_equals "$(get "$HELSINKI/name")" "Helsinki";
assert_equals "$(get "$STOCKHOLM/name")" "Stockholm";

# Link.
assert_equals "$(get "$HELSINKI/auth_$STOCKHOLM")" "true";
assert_equals "$(get "$STOCKHOLM/link_$HELSINKI")" "true";

# Check that service list is empty by default.
assert_equals "$(get "$HELSINKI/services")" "";
assert_equals "$(get "$STOCKHOLM/services")" "";
assert_equals "$(get "$HELSINKI/providers")" "";
assert_equals "$(get "$STOCKHOLM/providers")" "";

# Start an instance of the shout service.
./shoutservice.rb --port "$((PORT + 3))" &

SHOUT="$HOST:$((PORT + 3))";

wait_for_server "$SHOUT";

# Register the shout service
assert_equals "$(get "$HELSINKI/register_ShoutService_$SHOUT")" "true";

# We should now have the service registered.
assert_equals "$(get "$HELSINKI/services")" "ShoutService";
assert_equals "$(get "$STOCKHOLM/services")" "ShoutService";

# Check that both instances know the right providers for their services.
assert_equals "$(get "$HELSINKI/providers")" "ShoutService: http://$HELSINKI";
assert_equals "$(get "$STOCKHOLM/providers")" "ShoutService: http://$HELSINKI";

# Send a message to the service through Helsinki.
assert_equals "$(get --data "shout this string" "$HELSINKI/run_ShoutService")" "SHOUT THIS STRING";
assert_equals "$(get --data "WHISPER this string" "$HELSINKI/run_ShoutService_whisper")" "whisper this string";
assert_equals "$(get --data "python is the superior programming language" "$HELSINKI/run_ShoutService_sarcasm")" "pYtHoN Is tHe sUpErIoR PrOgRaMmInG LaNgUaGe";

# Send a message to the service from Stockholm through Helsinki.
assert_equals "$(get --data "shout this string" "$STOCKHOLM/run_ShoutService")" "SHOUT THIS STRING";
assert_equals "$(get --data "WHISPER this string" "$STOCKHOLM/run_ShoutService_whisper")" "whisper this string";
assert_equals "$(get --data "python is the superior programming language" "$STOCKHOLM/run_ShoutService_sarcasm")" "pYtHoN Is tHe sUpErIoR PrOgRaMmInG LaNgUaGe";

# Create a new Hobbes instance...
./hobbesd.rb --debug --port "$((PORT + 4))" --name Madrid &
MADRID="$HOST:$((PORT + 4))";
wait_for_server "$MADRID";
assert_equals "$(get "$MADRID")" "Hobbes";
assert_equals "$(get "$MADRID/name")" "Madrid";
assert_equals "$(get "$MADRID/services")" "";

# ...and link it to the network.
assert_equals "$(get "$STOCKHOLM/auth_$MADRID")" "true";
assert_equals "$(get "$MADRID/link_$STOCKHOLM")" "true";

# It should inherit all the services and providers.
assert_equals "$(get "$MADRID/services")" "ShoutService";
assert_equals "$(get "$MADRID/providers")" "ShoutService: http://$HELSINKI";

# Send a message to the service from Madrid through Helsinki.
assert_equals "$(get --data "shout this string" "$MADRID/run_ShoutService")" "SHOUT THIS STRING";
assert_equals "$(get --data "WHISPER this string" "$MADRID/run_ShoutService_whisper")" "whisper this string";
assert_equals "$(get --data "python is the superior programming language" "$MADRID/run_ShoutService_sarcasm")" "pYtHoN Is tHe sUpErIoR PrOgRaMmInG LaNgUaGe";

# Test that multiple arguments are handled properly.
assert_equals "$(get --data "I am " "$MADRID/run_ShoutService_append_handling_arguments_properly!")" "I am handling_arguments_properly!";

# Unregister the service.
assert_equals "$(get "$HELSINKI/unregister_$SHOUT")" "true";

# Check that the change propagated across the network.
assert_equals "$(get "$HELSINKI/services")" "";
assert_equals "$(get "$HELSINKI/providers")" "";
assert_equals "$(get "$STOCKHOLM/services")" "";
assert_equals "$(get "$STOCKHOLM/providers")" "";
assert_equals "$(get "$MADRID/services")" "";
assert_equals "$(get "$MADRID/providers")" "";

kill_all;
