PORT=25555;
HOST=localhost;

kill_all() {
	kill $(jobs -p);
}

exit() {
	kill_all;
	________; # what?
	command exit "$@";
}

get() {
	curl --silent "$@";
}

wait_for_server() {
	tries=20;
	while ! get "$1" >/dev/null; do
		tries=$((tries - 1));
		if ! [ "$tries" -gt 0 ]; then
			kill_all;
			exit 1;
		fi
		sleep 0.1;
	done
}

./hobbesd.rb --debug --port "$((PORT + 1))" --name Helsinki &
./hobbesd.rb --debug --port "$((PORT + 2))" --name Aberdeen &
./hobbesd.rb --debug --port "$((PORT + 3))" --name Phoenix &
./hobbesd.rb --debug --port "$((PORT + 4))" --name Taipei &

HELSINKI="$HOST:$((PORT + 1))";
ABERDEEN="$HOST:$((PORT + 2))";
PHOENIX="$HOST:$((PORT + 3))";
TAIPEI="$HOST:$((PORT + 4))";

# Wait for servers
wait_for_server "$HELSINKI";
wait_for_server "$ABERDEEN";
wait_for_server "$PHOENIX";
wait_for_server "$TAIPEI";

# Check sanity.
assert_equals "$(get "$HELSINKI")" "Hobbes";
assert_equals "$(get "$ABERDEEN")" "Hobbes";
assert_equals "$(get "$PHOENIX")" "Hobbes";
assert_equals "$(get "$TAIPEI")" "Hobbes";

assert_equals "$(get "$HELSINKI/name")" "Helsinki";
assert_equals "$(get "$ABERDEEN/name")" "Aberdeen";
assert_equals "$(get "$PHOENIX/name")" "Phoenix";
assert_equals "$(get "$TAIPEI/name")" "Taipei";

# Start instances of the shout service.
./shoutservice.rb --port "$((PORT + 5))" &
HELSINKISHOUTPID="$!";
./shoutservice.rb --port "$((PORT + 6))" &
ABERDEENSHOUTPID="$!";
./shoutservice.rb --port "$((PORT + 7))" &
PHOENIXSHOUTPID="$!";

HELSINKISHOUT="$HOST:$((PORT + 5))";
ABERDEENSHOUT="$HOST:$((PORT + 6))";
PHOENIXSHOUT="$HOST:$((PORT + 7))";

wait_for_server "$HELSINKISHOUT";
wait_for_server "$ABERDEENSHOUT";
wait_for_server "$PHOENIXSHOUT";

# Register the shout services for Helsinki and Aberdeen
assert_equals "$(get "$HELSINKI/register_ShoutService_$HELSINKISHOUT")" "true";
assert_equals "$(get "$ABERDEEN/register_ShoutService_$ABERDEENSHOUT")" "true";

# Link Helsinki and Aberdeen together
assert_equals "$(get "$HELSINKI/auth_$ABERDEEN")" "true";
assert_equals "$(get "$ABERDEEN/link_$HELSINKI")" "true";

# Use the service on Aberdeen.
assert_equals "$(get --data "shout this string" "$ABERDEEN/run_ShoutService")" "SHOUT THIS STRING";

# We should have a ShoutService registered, with two providers.
assert_equals "$(get "$HELSINKI/services")" "ShoutService";
assert_equals "$(get "$ABERDEEN/services")" "ShoutService";
assert_equals "$(get "$HELSINKI/providers")" "ShoutService: http://$HELSINKI http://$ABERDEEN";
assert_equals "$(get "$ABERDEEN/providers")" "ShoutService: http://$HELSINKI http://$ABERDEEN";

# Link Phoenix and Aberdeen together
assert_equals "$(get "$PHOENIX/auth_$ABERDEEN")" "true";
assert_equals "$(get "$ABERDEEN/link_$PHOENIX")" "true";

# Use the service through Phoenix.
assert_equals "$(get --data "shout this string" "$PHOENIX/run_ShoutService")" "SHOUT THIS STRING";

# Register the shout service for Phoenix.
assert_equals "$(get "$PHOENIX/register_ShoutService_$PHOENIXSHOUT")" "true";

# We should now have three providers for the shout service.
assert_equals "$(get "$HELSINKI/services")" "ShoutService";
assert_equals "$(get "$ABERDEEN/services")" "ShoutService";
assert_equals "$(get "$PHOENIX/services")" "ShoutService";
assert_equals "$(get "$HELSINKI/providers")" "ShoutService: http://$HELSINKI http://$ABERDEEN http://$PHOENIX";
assert_equals "$(get "$ABERDEEN/providers")" "ShoutService: http://$HELSINKI http://$ABERDEEN http://$PHOENIX";
assert_equals "$(get "$PHOENIX/providers")" "ShoutService: http://$HELSINKI http://$ABERDEEN http://$PHOENIX";

# Use the service on Phoenix.
assert_equals "$(get --data "shout this string" "$PHOENIX/run_ShoutService")" "SHOUT THIS STRING";

# Link Taipei to Phoenix.
assert_equals "$(get "$TAIPEI/auth_$PHOENIX")" "true";
assert_equals "$(get "$PHOENIX/link_$TAIPEI")" "true";

# Use the service on Taipei.
assert_equals "$(get --data "shout this string" "$TAIPEI/run_ShoutService")" "SHOUT THIS STRING";

# Kill 2/3 of the services.
kill "$HELSINKISHOUTPID" "$PHOENIXSHOUTPID";

# Use the service on Taipei a few times.
for i in 1 2 3 4 5 6 7; do
	assert_equals "$(get --data "shout this string" "$TAIPEI/run_ShoutService")" "SHOUT THIS STRING";
done

# Kill the last service.
kill "$ABERDEENSHOUTPID";

# Should return a "Bad Request" now that all services are down.
assert_find "$(get --data "shout this string" "$TAIPEI/run_ShoutService")" "error" "Bad Request";

kill_all;
