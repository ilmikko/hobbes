log() {
	echo "$@" 1>&2;
}

fail() {
	log "$@";
	exit 1;
}

[ -n "$HOBBES_SOCK" ] || HOBBES_SOCK="/tmp/hobbes.port";
[ -n "$HOBBES_SOCK_PID" ] || HOBBES_SOCK_PID="/tmp/hobbes.pid";

[ -f "$HOBBES_SOCK" ] && HOBBES_PORT="$(cat "$HOBBES_SOCK")";
HOBBESD="$DIR/hobbesd";

commands() {
	args="";
	while [ $# -gt 0 ]; do
		case $1 in # HELP Commands
			--connect) # Connect to another hobbes instance.
				# You can pass a hobbes address to --connect [address]
				# in order to send a connection request to the hobbes servers.
				shift;
				[ -n "$1" ] || fail "--connect needs a second argument.";
				args="$args $1";
				;;
			--help) # Show this help message.
				help;
				exit 0;
				;;
			--port) # Show the port local hobbes is running on.
				echo "$HOBBES_PORT";
				exit 0;
				;;
			--restart) # Restart the local hobbes server.
				[ -f "$HOBBES_SOCK" ] && rm "$HOBBES_SOCK";
				;;
			--stop) # Stop the local hobbes server.
				[ -f "$HOBBES_SOCK" ] && rm "$HOBBES_SOCK";
				if [ -f "$HOBBES_SOCK_PID" ]; then
					kill "$(cat "$HOBBES_SOCK_PID")"; # This does not kill the cat.
					rm "$HOBBES_SOCK_PID";
				fi
				exit 0;
				;;
			--*)
				fail "Unknown command: $1";
				;;
			--)
				break;
				;;
		esac # END HELP
		shift;
	done

	"$HOBBESD" $args || exit 1;
}

arguments() {
	while [ $# -gt 0 ]; do
		case $1 in
			--*)
				;;
			--|*)
				break;
				;;
		esac
		shift;
	done

	hobbes_send "$@";
}

hobbes_send() {
	args="$(echo "$@" | tr ' ' '_')";
	curl --silent --fail "localhost:$HOBBES_PORT/$args" || exit 1;
	echo;
}

main() {
	commands "$@";
	arguments "$@";
}
