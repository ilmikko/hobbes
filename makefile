test: client_test server_test

client_test:
	cd client && tester $(1) $(2) $(3)

server_test:
	cd server && tester $(1) $(2) $(3)
