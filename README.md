# Hobbes

Hobbes is a distributed message passing system for RPCs.
It communicates over HTTP, passing general messages to all of the nodes.

A request to a Hobbes node "hops" from one to another (hence the name).
Any node that has already seen a given message will not pass it along any further.
This means messages never traverse back the way they come, but propagate across an unknown sized network.

## Example

```
Client
 | (service X?)
 v
Node A ---> Node B ---> Node C (service X)
```

Here we can see that a Client is requesting node A for service X.
The request hops two times until it hits node C which will respond to it.
The client will then receive a response through node A of service X - this hopping is completely transparent to the user.

Note how the order of the network does not really matter here.
If latency is an issue, we can simply connect another node D with service X to the network:

```
Client
 | (service X?)
 v
Node A  --  Node B  --  Node C
 |
 v
Node D (service X)
```

This causes the request to propagate to node D and get returned back faster.
All that is needed for set-up is for node D to be attached into the network.
Node A will update its "neighbors" but nothing else changes.

When the client sends a request, the fastest service will respond.
Note that the services should be stateless - Hobbes does not handle state.
